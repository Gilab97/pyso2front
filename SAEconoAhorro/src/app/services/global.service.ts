import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  public tipo: number | undefined;
  public id: number | undefined;
  public nombre: string | undefined;
  public correo: string | undefined;

  public universal = false;

  public Grupos = [
    {nombre: 'Grupo 1', API: 'http://busg1.us-e2.cloudhub.io/'},
    {nombre: 'Grupo 2', API: ''},
    {nombre: 'Grupo 3', API: 'http://35.206.98.190/'},
    {nombre: 'Grupo 4', API: 'http://esb4.djgg.ml:3030/'},
    {nombre: 'Grupo 5', API: 'http://34.123.238.63:8280/services/integrador/'},
    {nombre: 'Grupo 6', API: 'http://35.184.63.236:3004/'},
    {nombre: 'Grupo 7', API: 'http://68.183.102.104:3000/'},
    {nombre: 'Grupo 8', API: 'http://35.232.242.252:9999/'},
    {nombre: 'Grupo 9', API: 'http://sa-g9.us-e2.cloudhub.io/'},
    {nombre: 'Grupo 10', API: 'http://34.73.157.172:5005/'},
    {nombre: 'Grupo 11', API: 'http://soagrupo11.us-e2.cloudhub.io/ver-productos/'},
    {nombre: 'Grupo 12', API: ''},
    {nombre: 'Grupo 13', API: 'http://www.sa-proyecto.tk/'},
    {nombre: 'Grupo 14', API: 'http://35.226.247.82:8080/'},
    {nombre: 'Grupo 15', API: 'http://34.73.17.174:4000/'}
  ]

  //public API = "http://35.232.163.112/"
  public API_BUS = 'http://35.232.242.252:9999/'

  constructor() { }
}
