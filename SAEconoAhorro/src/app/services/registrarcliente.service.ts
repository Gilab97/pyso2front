import { Injectable } from '@angular/core';
import {cliente} from '../models/regcliente';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GlobalService} from "./global.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin':'*'
  })
};

@Injectable({
  providedIn: 'root'
})

export class RegistrarclienteService {

  private API = 'http://35.232.242.252:9999/';
  constructor(private http: HttpClient, public globalService: GlobalService) { }

  registrar(request: cliente){
    return this.http.post(`${this.API}registrar-cliente`, request, httpOptions);
  }

}
