import { TestBed } from '@angular/core/testing';

import { TarjetasService } from './tarjetas.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('TarjetasService', () => {
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: TarjetasService = TestBed.get(TarjetasService);
    expect(service).toBeTruthy();
  });
});
