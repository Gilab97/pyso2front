import { TestBed } from '@angular/core/testing';

import { ProviderService } from './provider.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('ProviderService', () => {
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: ProviderService = TestBed.get(ProviderService);
    expect(service).toBeTruthy();
  });
});
