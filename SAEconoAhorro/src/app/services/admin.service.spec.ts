import { TestBed } from '@angular/core/testing';

import { AdminService } from './admin.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('AdminService', () => {
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: AdminService = TestBed.get(AdminService);
    expect(service).toBeTruthy();
  });
});
