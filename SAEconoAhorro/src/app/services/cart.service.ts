import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Login } from '../models/Login';
import { venta , ventaState } from '../models/Venta';
import { detalleVenta } from '../models/Detalle';
import { updateCarrito } from '../models/Update';
import { email } from '../models/Email';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private API = 'http://35.232.242.252:9999/';

  constructor(private http: HttpClient) { }

  registrarVenta(request: venta){
    return this.http.post(`${this.API}shopping/create-sell`,request,httpOptions);
  }
  registrarDetalle(request: detalleVenta){
    return this.http.post(`${this.API}shopping/create-detail`,request,httpOptions);
  }
  registrarUpdateStock(request: updateCarrito ){
    return this.http.post(`${this.API}shopping/updateInventory`,request,httpOptions);
  }
  registrarUpdateVenta(request: ventaState){
    return this.http.post(`${this.API}update-venta`,request,httpOptions)
  }
  sendEmailConfirmation(email: email ){
    return this.http.post('https://dev.api.kolo-app.com/api/nsmain?token=648523452385',email,httpOptions);
  }

}
