import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GlobalService} from './global.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private API = 'http://35.232.242.252:9999';

  constructor(private http: HttpClient, public globalService: GlobalService) { }

  updateStatus(id: number, data: any){
    return this.http.put(`${this.API}/admin/${id}`, data, httpOptions)
  }

  getSales(params: {id?: string}){
    // @ts-ignore
    return this.http.get(`${this.API}/admin`, {params})
  }

  getSalesById(id: number){
    return this.http.get(`${this.API}/admin/${id}`)
  }
}
