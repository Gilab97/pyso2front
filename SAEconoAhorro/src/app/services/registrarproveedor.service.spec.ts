import { TestBed } from '@angular/core/testing';

import { RegistrarproveedorService } from './registrarproveedor.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('RegistrarproveedorService', () => {
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: RegistrarproveedorService = TestBed.get(RegistrarproveedorService);
    expect(service).toBeTruthy();
  });
});
