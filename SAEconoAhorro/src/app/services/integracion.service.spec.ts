import { TestBed } from '@angular/core/testing';

import { IntegracionService } from './integracion.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('IntegracionService', () => {
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: IntegracionService = TestBed.get(IntegracionService);
    expect(service).toBeTruthy();
  });
});
