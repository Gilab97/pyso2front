import { TestBed } from '@angular/core/testing';

import { CartService } from './cart.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {LoginService} from './login.service';

describe('CartService', () => {
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: CartService = TestBed.get(CartService);
    expect(service).toBeTruthy();
  });
});
