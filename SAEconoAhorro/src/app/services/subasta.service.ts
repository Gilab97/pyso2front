import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subasta} from '../models/Subasta';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class SubastaService {

  private API = 'http://35.232.242.252:9999/';

  constructor(private http: HttpClient) { }

  getSubastas(){
    return this.http.get(`${this.API}auction`);
  }

  getSubastasById(id: number){
    return this.http.get(`${this.API}auction/${id}`);
  }

  crearSubasta(request: Subasta){
    return this.http.post(`${this.API}auction`, request, httpOptions);
  }

  crearPuja(request: any){
    return this.http.post(`${this.API}auction/bid`, request, httpOptions);
  }
}
