import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Login } from '../models/Login';
import {GlobalService} from "./global.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // private API = 'http://35.232.242.252:9999/';
  private API = 'http://35.192.106.128:3030/';

  constructor(private http: HttpClient, public globalService: GlobalService) { }

  registrar(request: Login){
    return this.http.post(`${this.API}login-cliente`, request, httpOptions);
  }

}
