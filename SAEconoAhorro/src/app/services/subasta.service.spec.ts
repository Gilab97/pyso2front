import { TestBed } from '@angular/core/testing';

import { SubastaService } from './subasta.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('SubastaService', () => {
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: SubastaService = TestBed.get(SubastaService);
    expect(service).toBeTruthy();
  });
});
