import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Login } from '../models/Login';
import {GlobalService} from "./global.service";
import {cliente} from "../models/regcliente";
import {prov} from "../models/regproveedor";

const httpOptions = {
  headers: {
    'Content-Type': 'application/json',
  },
  mode: 'no-cors',
  credential: 'omit'
};

@Injectable({
  providedIn: 'root'
})
export class IntegracionService {

  //private API = 'http://35.188.53.22:3030/';

  constructor(private http: HttpClient, public globalService: GlobalService) { }

  login_cliente(request: Login){
    return this.http.post(`${this.globalService.API_BUS}login-cliente`, request, httpOptions);
  }

  login_proveedor(request: Login){
    return this.http.post(`${this.globalService.API_BUS}login-proveedor`, request, httpOptions);
  }

  registrar_cliente(request: cliente){
    return this.http.post(`${this.globalService.API_BUS}registrar-cliente`, request, httpOptions);
  }

  registrar_proveedor(request: prov){
    return this.http.post(`${this.globalService.API_BUS}registrar-proveedor`, request, httpOptions);
  }

  cargar_imagen(request: any){
    return this.http.post(`http://35.188.53.22:3015/api/products/imagen`, request, httpOptions);
  }

  crear_producto(request: any){
    return this.http.post(`${this.globalService.API_BUS}crear-producto-proveedor`, request, httpOptions);
  }

  ver_productos(){
    return this.http.get (`${this.globalService.API_BUS}ver-productos`, httpOptions);
  }

  realizar_compra(request: any){
    return this.http.post(`${this.globalService.API_BUS}realizar-compra`, request, httpOptions);
  }
}
