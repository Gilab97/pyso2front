import { TestBed } from '@angular/core/testing';

import { RegistrarclienteService } from './registrarcliente.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('RegistrarclienteService', () => {
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: RegistrarclienteService = TestBed.get(RegistrarclienteService);
    expect(service).toBeTruthy();
  });
});
