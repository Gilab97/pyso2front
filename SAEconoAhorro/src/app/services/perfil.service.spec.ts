import { TestBed } from '@angular/core/testing';

import { PerfilService } from './perfil.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('PerfilService', () => {
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: PerfilService = TestBed.get(PerfilService);
    expect(service).toBeTruthy();
  });
});
