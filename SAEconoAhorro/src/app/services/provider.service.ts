import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  private API = 'http://35.232.242.252:9999/';

  constructor(private http: HttpClient) { }

  getProviders(){
    return this.http.get(`${this.API}provider`);
  }

  getProductsByProvider(id: number | undefined){
    return this.http.get(`${this.API}provider/${id}/products`);
  }
}
