import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Product} from '../models/Product';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private API = 'http://35.232.242.252:9999/';

  constructor(private http: HttpClient) { }

  create(request: Product){
    return this.http.post(`http://35.188.53.22:3015/api/products/`, request, httpOptions);
  }

  getAllProducts(params: any){
    return this.http.get(`${this.API}products`, {params});
  }

  update(request: Product, id: number){
    return this.http.put(`${this.API}products/${id}`, request, httpOptions);
  }

  getSingle(id: number){
    return this.http.get(`${this.API}products/${id}`);
  }

  getCategories(){
    return this.http.get(`${this.API}products/categories`);
  }

  getFavorite(params: any, id: number){
    return this.http.get(`http://35.188.53.22:3015/api/products/favorite/${id}`, {params});
  }

  setFavorite(body: any){
    return this.http.post(`http://35.188.53.22:3015/api/products/favorite`, body, httpOptions);
  }

  removeFavorite(body: any){
    return this.http.post(`http://35.188.53.22:3015/api/products/favorite/remove`, body, httpOptions);
  }
}
