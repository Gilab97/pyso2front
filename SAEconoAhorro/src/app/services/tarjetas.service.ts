import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {GlobalService} from "./global.service";

@Injectable({
  providedIn: 'root'
})
export class TarjetasService {

  private API = 'http://35.188.53.22:3020';
  constructor(private http: HttpClient, private  Global: GlobalService) { }

  public getCards(user: any){
    return this.http.get(`${this.API}/api/card/${user}`, user);
  }

  public newCard(objeto: any){
    return this.http.post(`${this.API}/api/card/`, objeto);
  }
}
