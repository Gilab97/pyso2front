import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Perfil} from '../models/Perfil';
import {GlobalService} from "./global.service";


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})



export class PerfilService {
  private API = 'http://35.232.242.252:9999/';
  constructor(private http: HttpClient, private  Global: GlobalService) { }

  public getProfile(user: any){
    return this.http.get(`${this.API}users/client/${user}`, user);
  }

  public getProviderProfile(user: any){
    return this.http.get(`${this.API}users/provider/${user}`, user);
  }

  public updateProfile(request: Perfil){
    return this.http.put(`${this.API}users/client/${this.Global.id}`, request);
  }

  public updateProviderProfile(request: any){
    return this.http.put(`${this.API}users/provider/${this.Global.id}`, request);
  }

  public updatePictura(object: any){
    return this.http.put(`${this.API}users/profile/picture`, object);
  }

  public getReport(user: any){
    return this.http.get(`${this.API}users/report/${user}`, user);
  }

  public getProductsClient(user: any){
    return this.http.get(`${this.API}users/client/report/${user}`, user)
  }

  public getProductsProvider(user: any){
    return this.http.get(`${this.API}users/provider/report/${user}`, user)
  }

  public getSubastaClient(user: any){
    return this.http.get(`${this.API}users/client/subasta/${user}`, user)
  }

  public getSubastaProvider(user: any){
    return this.http.get(`${this.API}users/provider/subasta/${user}`, user)
  }
}
