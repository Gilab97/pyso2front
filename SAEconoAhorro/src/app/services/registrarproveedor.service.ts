import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { prov } from '../models/regproveedor';
import {GlobalService} from "./global.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RegistrarproveedorService {

  private API = 'http://35.232.242.252:9999/';

  constructor(private http: HttpClient, public globalService: GlobalService) { }

  public registrar(request: prov){
    return this.http.post(`${this.API}registrar-proveedor`, request, httpOptions);
  }

}
