import {Product} from '../models/Product';
import moment from "moment";

export function formatNumberWithCommas(x: any): any{
  return (x !== null || !typeof x !== undefined) ? parseFloat(x).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : '';
}

export function addCart(item: Product, id: number): any{
  const cart = getCart(id)
  const index = cart.findIndex((c: Product) => c.id === item.id)

  if(index!==-1 && item.select){
    cart.map((p: Product) => {if(p.id === item.id) p.quantity = item.quantity})
  }
  else if(index !== -1 && !item.select){
    cart.splice(index, 1);
  }
  else{
    cart.push(item)
  }

  window.localStorage.setItem(`CART_LOCAL_STORAGE-${id}`, JSON.stringify(cart))
}

export function getCart(id: number): any{
  const response = window.localStorage.getItem(`CART_LOCAL_STORAGE-${id}`)
  let cart = []
  if(response){
    cart = JSON.parse(response);
  }
  return cart
}

export function formatDateFromMillis(millis: any){
  const day = moment(millis);
  return moment(day).format('DD/MM/YYYY hh:mm a');
};
