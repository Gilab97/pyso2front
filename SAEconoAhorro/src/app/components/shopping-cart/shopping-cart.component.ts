import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { getCart, addCart} from '../../utilities/utilities'
import {Product} from '../../models/Product';
import {GlobalService} from '../../services/global.service';
import { PdfMakeWrapper,Img , Table,Txt  } from 'pdfmake-wrapper';
import {IntegracionService} from "../../services/integracion.service";


import { DatePipe } from '@angular/common'
import { CartService } from "../../services/cart.service"
import { TarjetasService } from "../../services/tarjetas.service";

// @ts-ignore
import pdfFonts from "pdfmake/build/vfs_fonts"; // fonts provided for pdfmake
PdfMakeWrapper.setFonts(pdfFonts);

// tslint:disable-next-line:class-name
export interface tarjeta{
  numeroTarjeta: number,
  cvc: number
  nombre: string,
}

export interface Cliente{
  id: number,
  nombre: string,
  apellido: string,
  correo: string,
  celular: string
}
export interface Venta{
  fecha: string,
  cantTotal: number,
  total: number,
  cliente: Cliente
}
export interface Detalle{
  venta: Venta,
  producto: number,
  productoStr: string,
  cantidad: number,
  subTotal: number,
}
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})

export class ShoppingCartComponent implements OnInit {
  cliente: any;
  tarjetas: any = [] ;
  carrito: any = [];
  tarjetaActual: any;
  products: Product[] = [];
  cantTotalActual = 0;
  totalActual = 0;
  direccion = '';
  entrega = true;
  constructor(private toastr: ToastrService, public global: GlobalService,
              private cartService: CartService,public datepipe: DatePipe, private tarjetaService: TarjetasService,
              public integracion: IntegracionService) {
  }

  ngOnInit(): void {
    this.cliente = {nombre: this.global.nombre, correo: this.global.correo, id: this.global.id}
    this.carrito = getCart(this.global.id || 0);
    this.tarjetaService.getCards(this.cliente.id).subscribe((res: any)=>{
      this.tarjetas = res;
    });
  }
  removeProduct(item: Product): void {
    item.select = !item.select
    addCart(item, this.global.id || 0)
    this.carrito = getCart(this.global.id || 0)
  }

  regVenta(){
    this.getTotalActual();
    const dateOb = new Date();
    let ventaActual: any;
    const latestDate = this.datepipe.transform(dateOb, 'yyyy-MM-dd');
    console.log(latestDate);


    this.cartService.registrarVenta({ fecha: latestDate?.toString(), cantTotal: this.cantTotalActual, total: this.totalActual,
       cliente: 1 , entrega: this.entrega ? 1 : 0, estado: 1}).subscribe((res: any)=>{
        ventaActual = res.insertId;

        this.cartService.sendEmailConfirmation({body:'Su compra ha sido realizada, su tracking de venta es:'+ventaActual,
          email:this.cliente.correo,header:'Venta ' + latestDate,subject:'Notificaciones Econo Ahorro'}).subscribe((res=>{
          console.log('email response'+res);
        }));

        this.carrito.forEach((element: any) =>  {
          this.cartService.registrarDetalle({venta:ventaActual,cantidad:element.quantity, producto:element.id,subTotal:
           element.precio, subasta: element.subasta}).subscribe((res: any)=>{
            console.log(res);
            if(!element.subasta){
              this.cartService.registrarUpdateStock({id:element.id,quantity:element.quantity}).subscribe((res: any)=>{
                console.log(res);
                this.removeProduct(element);
              });
            }
            else this.removeProduct(element);
         });
        });
    });
    this.printInvoice();


  }

  regVentaUniversal(){
    const products: any = []
    this.carrito.forEach((element: any) => {
      console.log(element)
      const obj = {
        id_producto: element.id_producto,
        cantidad: element.quantity
      }
      products.push(obj)
    })
    console.log(products)
    const objeto = {
      id_cliente: this.global.id,
      productos: products
    }
    console.log(objeto)
    this.integracion.realizar_compra(objeto).subscribe(
      res => {
        console.log(res)

      }, error => {
        console.log(error)
    }
    )
    this.carrito.forEach((element: any) =>  {
      console.log(element)
      this.removeProduct(element);
    });
  }

  getTotalActual(){
    this.carrito.forEach((element: any) => {
      this.totalActual += (element.precio*element.quantity);
      this.cantTotalActual += element.quantity;
    });
  }
  async printInvoice() {

    // tslint:disable-next-line:variable-name
    const date_ob = new Date()
    const pdf = new PdfMakeWrapper();
    pdf.add(new Txt('Factura Econo ahorro!').bold().end);
    pdf.add('                          ' +  this.datepipe.transform(date_ob, 'dd/MM/YYYY'));
    pdf.add('                          ');
    pdf.add('___________________________________________________________________________________________');
    pdf.add('                          ');
    pdf.add('    Nombre: '+this.cliente.nombre +'                                     Nit:c/f');
    pdf.add('    Correo: '+this.cliente.correo);
    pdf.add('___________________________________________________________________________________________');
    pdf.add('                          ');
    pdf.add(new Txt('Descripcion                        Cantidad                        Total').bold().end);
    this.carrito.forEach((element: any) => {
      pdf.add(element.nombre+'                        '+element.quantity+'                                   '+element.precio);
    });
    pdf.add('                          ');
    pdf.add(new Txt('__________________________________________________________________________________________').bold().end);
    pdf.add(new Txt('Total        '+this.totalActual).alignment('right').italics().end);
    pdf.footer("                              Econo footer")
    pdf.create().download();
    this.toastr.success('Compra Exitosa', 'Detalle compra');
  }

  defineCard(value: any): void{
    console.log(value);
    this.toastr.success( value,'Tarjeta Seleccionada');
    this.tarjetaActual = value;
  }
}
