import { Component, OnInit } from '@angular/core';
import {ProviderService} from '../../services/provider.service';
import {GlobalService} from '../../services/global.service';
import {Product} from '../../models/Product';
import {formatNumberWithCommas} from '../../utilities/utilities'
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ProductsService} from '../../services/products.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ProductTableComponent implements OnInit {

  formatNumber = formatNumberWithCommas

  displayedColumns = ['nombre', 'categoria', 'stock', 'precio'];
  dataSource: Product[] = [];
  expandedElement: Product = {};
  updateElement: Product = {};

  categories: {id: number, nombre: string}[] = [{id: 1, nombre: 'asdf'}]
  category = {id: 0, nombre: ''}

  constructor(private providerService: ProviderService, public globalService: GlobalService, private productService: ProductsService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.providerService.getProductsByProvider(this.globalService.id).subscribe(
      (res: Product[] | any) => {
      this.dataSource = [...res, {id: 0}];
    },error => console.error(error))

    this.productService.getCategories().subscribe((res: any) => {
      this.categories = res
    })
  }

  showProduct(expandedElement: any, row: any): void{
    this.category={id: row.categoria_id, nombre: row.categoria}
    this.updateElement = this.expandedElement = expandedElement === row ? null : row
  }

  onCategoryChange(event: any, item: any): void{
    if(event.isUserInput){
      this.category = item
    }
  }

  updateProduct(): void{
    if(this.updateElement.id !== 0){
      const request = {nombre: this.updateElement.nombre, categoria_id: this.category.id, precio: this.updateElement.precio,
        stock: this.updateElement.stock}
      this.productService.update(request, this.updateElement.id || 0).subscribe((res: any) => {
        this.toastr.success( 'Producto actualizado',this.updateElement.nombre);
      }, error => console.error(error))
    }
    else this.createProduct()
  }

  createProduct(): void{
    const request = {nombre: this.updateElement.nombre, categoria_id: this.category.id, precio: this.updateElement.precio,
      stock: this.updateElement.stock, proveedor_id: this.globalService.id}
    this.productService.create(request).subscribe((res: any) => {
      this.toastr.success( 'Producto creado',this.updateElement.nombre);
    }, error => console.error(error))
  }

}
