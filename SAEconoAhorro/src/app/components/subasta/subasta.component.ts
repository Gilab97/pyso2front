import { Component, OnInit } from '@angular/core';
import {Product} from '../../models/Product';
import {Subasta} from '../../models/Subasta';
import {ActivatedRoute} from '@angular/router';
import {ProductsService} from '../../services/products.service';
import {GlobalService} from '../../services/global.service';
import {SubastaService} from '../../services/subasta.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-subasta',
  templateUrl: './subasta.component.html',
  styleUrls: ['./subasta.component.css']
})
export class SubastaComponent implements OnInit {

  product: Product = {}

  subasta: Subasta = {}

  constructor(private activatedRoute: ActivatedRoute, private productService: ProductsService, public globalService: GlobalService,
              private subastaService: SubastaService, private toastr: ToastrService) {

  }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id') || 0;
    this.productService.getSingle(Number(id)).subscribe((res: Product[] | any) => {
      this.product = res[0]
    }, error => console.error(error))
  }

  crearSubasta(): void{
    const tiempo = new Date().setHours(this.subasta.hora || 0, this.subasta.minuto || 0, 0, 0)
    this.subastaService.crearSubasta({tiempo, titulo: this.subasta.titulo, descripcion: this.subasta.descripcion,
      precio_inicial: this.subasta.precio_inicial, proveedor_id: this.globalService.id, producto_id: this.product.id}).subscribe((res) => {
      this.toastr.success("Se creó la subasta satisfactoriamente", "Subasta")
    }, error =>  console.error(error))
  }
}
