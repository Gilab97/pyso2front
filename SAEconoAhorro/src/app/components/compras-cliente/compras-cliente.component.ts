import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {PerfilService} from "../../services/perfil.service";
import {GlobalService} from "../../services/global.service";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-compras-cliente',
  templateUrl: './compras-cliente.component.html',
  styleUrls: ['./compras-cliente.component.css']
})
export class ComprasClienteComponent implements OnInit {

  Datos: any
  Subasta: any

  constructor(private router: Router, private perfilService: PerfilService, public Global: GlobalService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.perfilService.getProductsClient(this.Global.id).subscribe(
      res=>{
        this.Datos = res
        console.log(this.Datos)
      }, err=>{
        console.log(err)
      }
    )

    this.perfilService.getSubastaClient(this.Global.id).subscribe(
      res=>{
        this.Subasta = res
        console.log(this.Subasta)
      }, err=>{
        console.log(err)
      }
    )
  }

}
