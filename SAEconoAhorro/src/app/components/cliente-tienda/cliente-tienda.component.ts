import { Component, OnInit } from '@angular/core';
import {addCart, formatNumberWithCommas, getCart} from "../../utilities/utilities";
import {Product} from "../../models/Product";
import {ProductsService} from "../../services/products.service";
import {ProviderService} from "../../services/provider.service";
import {GlobalService} from "../../services/global.service";
import {IntegracionService} from "../../services/integracion.service";
import {PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-cliente-tienda',
  templateUrl: './cliente-tienda.component.html',
  styleUrls: ['./cliente-tienda.component.css']
})
export class ClienteTiendaComponent implements OnInit {

  formatNumber = formatNumberWithCommas
  loading = true
  cart = []
  idUser: any = 0

  skip = 0
  size = 10
  totalProducts: any = []
  products: any = []
  count = 0

  constructor(private integracion: IntegracionService, public globalService: GlobalService) {
  }

  ngOnInit(): void {
    this.idUser = this.globalService.id
    this.cart = getCart(this.globalService.id || 0)
    console.log(this.cart);
    this.setup()
  }

  setup(): void{
    this.loading = true
    this.integracion.ver_productos().subscribe(
      (response: any) => {
        response.data.map((r: any) => {
          this.cart.map((c: any) => {
            if(c.id === r.id_producto){
              r.quantity = c.quantity
              r.select = c.select
            }
          })
        })
        this.totalProducts = response.data
        this.products = response.data.slice(0,20)
        this.count = response.data.length
        setTimeout(() => {this.loading = false},500)
      },error => {console.error(error); this.loading = false})
  }

  reduceQuantity(item: any): void{
    if(item.quantity !==undefined && item.quantity > 1){
      item.quantity = item.quantity - 1
    }
    else if(item.quantity === undefined) item.quantity = 1
    else item.select = !item.select

    addCart({...item, id: item.id_producto}, this.globalService.id || 0)
    this.cart = getCart(this.globalService.id || 0)
  }

  addQuantity(item: any): void{
    if(item.quantity !== undefined && item.quantity < (item.stock || 0)){
      item.quantity = item.quantity + 1
    }
    else if(item.quantity === undefined) item.quantity = 1
    else item.quantity = item.stock

    addCart({...item, id: item.id_producto}, this.globalService.id || 0)
    this.cart = getCart(this.globalService.id || 0)
  }

  addToCart(product: any): void{
    product.select = !product.select
    product.quantity = 1;

    addCart({...product, id: product.id_producto}, this.globalService.id || 0)
    this.cart = getCart(this.globalService.id || 0)
  }

  pageEvent(event: PageEvent): void{
    const pos = (event.pageIndex+1) * event.pageSize
    this.products = this.totalProducts.slice(pos - event.pageSize, (event.pageIndex+1) * event.pageSize)
  }
}
