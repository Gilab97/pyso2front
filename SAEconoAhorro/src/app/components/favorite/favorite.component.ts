import { Component, OnInit } from '@angular/core';
import {addCart, formatNumberWithCommas, getCart} from "../../utilities/utilities";
import {Product} from "../../models/Product";
import {ProductsService} from "../../services/products.service";
import {ProviderService} from "../../services/provider.service";
import {GlobalService} from "../../services/global.service";
import {PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {
  formatNumber = formatNumberWithCommas
  loading = true
  cart = []
  idUser: any = 0

  skip = 0
  size = 10
  products: Product[] = []
  providers: {id: number, nombre: string}[] = [{id: 1, nombre: 'asdf'}]
  provider = {id: 0}
  categories: {id: number, nombre: string}[] = [{id: 1, nombre: 'asdf'}]
  category = {id: 0}
  count = 0

  constructor(private productService: ProductsService, private providerService: ProviderService, public globalService: GlobalService) {
  }

  ngOnInit(): void {
    this.idUser = this.globalService.id
    this.cart = getCart(this.globalService.id || 0)
    this.productService.getCategories().subscribe((res: any) => {
      this.categories = [{id: 0, nombre: 'Seleccionar todas'}, ...res]
    }, error => console.error(error))
    this.providerService.getProviders().subscribe((res: any) => {
      this.providers = [{id: 0, nombre: 'Seleccionar todos'}, ...res]
    }, error => console.error(error))
    this.setup()
  }

  setup(): void{
    this.loading = true
    const params: {skip: number, size: number, category?: number, provider?: number} = {skip: 0, size: 0}
    params.skip = this.skip
    params.size = this.size
    if(this.category.id !== 0)  params.category = this.category.id
    if(this.provider.id !== 0)  params.provider = this.provider.id
    this.productService.getFavorite(params, this.idUser).subscribe(
      (response: {rows: Product[], count: number} | any) => {
        response.rows.map((r: Product) => {
          this.cart.map((c: any) => {
            if(c.id === r.id){
              r.quantity = c.quantity
              r.select = c.select
            }
          })
        })
        this.products = response.rows
        this.count = response.count
        setTimeout(() => {this.loading = false},500)
      },error => {console.error(error); this.loading = false})
  }

  removeFavorite(item: Product): void{
    const object: any = {usuario: this.idUser, producto: item.id}
    console.log(object)
    this.productService.removeFavorite(object).subscribe(
      res => {
        console.log(res)
        alert("Producto eliminado de favoritos")
        this.ngOnInit()
      }
    )
  }

  reduceQuantity(item: Product): void{
    if(item.quantity !==undefined && item.quantity > 1){
      item.quantity = item.quantity - 1
    }
    else if(item.quantity === undefined) item.quantity = 1
    else item.select = !item.select

    addCart(item, this.globalService.id || 0)
    this.cart = getCart(this.globalService.id || 0)
  }

  addQuantity(item: Product): void{
    if(item.quantity !== undefined && item.quantity < (item.stock || 0)){
      item.quantity = item.quantity + 1
    }
    else if(item.quantity === undefined) item.quantity = 1
    else item.quantity = item.stock

    addCart(item, this.globalService.id || 0)
    this.cart = getCart(this.globalService.id || 0)
  }

  pageEvent(event: PageEvent): void{
    this.skip = event.pageIndex * event.pageSize
    this.size = event.pageSize
    this.setup()
  }

  addToCart(product: Product): void{
    product.select = !product.select
    product.quantity = 1;

    addCart(product, this.globalService.id || 0)
    this.cart = getCart(this.globalService.id || 0)
  }

  onProviderChange(event: any, item: any): void{
    if(event.isUserInput){
      this.skip = 0
      this.provider = item
      this.setup()
    }
  }

  onCategoryChange(event: any, item: any): void{
    if(event.isUserInput){
      this.skip = 0
      this.category = item
      this.setup()
    }
  }
}

