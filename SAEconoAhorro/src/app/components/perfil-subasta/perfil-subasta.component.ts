import {Component, OnDestroy, OnInit} from '@angular/core';
import {Product} from '../../models/Product';
import {Subasta} from '../../models/Subasta';
import {GlobalService} from '../../services/global.service';
import {ProductsService} from '../../services/products.service';
import {SubastaService} from '../../services/subasta.service';
import {ActivatedRoute} from '@angular/router';
import {formatDateFromMillis, addCart} from '../../utilities/utilities';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-perfil-subasta',
  templateUrl: './perfil-subasta.component.html',
  styleUrls: ['./perfil-subasta.component.css']
})
export class PerfilSubastaComponent implements OnInit, OnDestroy {

  formatDate = formatDateFromMillis

  product: Product = {}

  subasta: Subasta = {}

  valor = 0

  bids: any = []

  id: any

  interval: any;

  habilitado = true

  displayedColumns: string[] = ['fotografia','nombres', 'valor'];

  constructor(public globalService: GlobalService, private productService: ProductsService, private subastaService: SubastaService,
              private activatedRoute: ActivatedRoute, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id') || 0;
    this.subastaService.getSubastasById(Number(this.id)).subscribe((res: {bids: [], rows: []} | any) => {
      this.subasta = res.rows[0]
      this.bids = res.bids
      const date = new Date().valueOf()
      if(date > (this.subasta.tiempo || 0)){
        this.habilitado = false
        if(this.bids.length > 0){
          const lastBid = this.bids[this.bids-1]
          if(this.globalService.id === lastBid.usuario_id){
            const product = {...this.product, quantity: 1, select: true, subasta: this.id, precio: lastBid.valor}
            addCart(product, this.globalService.id || 0)
          }
        }
      }
      this.productService.getSingle(this.subasta.producto_id || 0).subscribe((r: Product[] | any) => {
        this.product = r[0]
      })
    }, error => console.error(error))

    this.interval = setInterval(() => {
      this.setup(Number(this.id))
    }, 5000)
  }

  setup(id: number): void{
    this.subastaService.getSubastasById(Number(id)).subscribe((res: any) => {
      this.subasta = res.rows[0]
      this.bids = res.bids
      const date = new Date().valueOf()
      if(date > (this.subasta.tiempo || 0)){
        this.habilitado = false
        if(this.bids.length > 0){
          const lastBid = this.bids[0]
          if(this.globalService.id === lastBid.usuario_id){
            const product = {...this.product, quantity: 1, select: true, subasta: this.id, precio: lastBid.valor}
            addCart(product, this.globalService.id || 0)
          }
        }
      }
    }, error => console.error(error))
  }

  ngOnDestroy() {
    clearInterval(this.interval)
  }

  createBid(): void{
    this.subastaService.crearPuja({valor: this.valor, usuario_id: this.globalService.id , subasta_id: Number(this.id)}).subscribe(
      (res) =>{
        this.setup(this.id)
      }, error => this.toastr.error("No se pudo registrar el valor", "Error")
    )
  }

}
