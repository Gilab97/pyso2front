import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilSubastaComponent } from './perfil-subasta.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {formatNumberWithCommas, addCart, getCart, formatDateFromMillis} from '../../utilities/utilities'
import {Product} from '../../models/Product';

describe('PerfilSubastaComponent', () => {
  let component: PerfilSubastaComponent;
  let fixture: ComponentFixture<PerfilSubastaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilSubastaComponent ],
      imports: [RouterTestingModule, HttpClientTestingModule, ToastrModule.forRoot()],
      providers: [ToastrService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilSubastaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create bid', () => {
    component.createBid()
    expect(true).toBeTruthy();
  });

  it('should create setUp', () => {
    component.setup(1)
    expect(true).toBeTruthy();
  });

  it('should getCart', () => {
    getCart(1)
    expect(true).toBeTruthy();
  });

  it('should number with commas', () => {
    formatNumberWithCommas(1)
    expect(true).toBeTruthy();
  });

  it('should add cart', () => {
    const product: Product = {quantity: 0, precio: 0, id: 0, proveedor: '', categoria: '', select: true, nombre: '', stock: 0,
      categoria_id: 0, proveedor_id: 0, imagen: '', subasta: 10}
    addCart(product, 1)
    expect(getCart(1)).toBeDefined();
  });
});
