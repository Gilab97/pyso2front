import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {GlobalService} from "../../services/global.service";
import {Login} from "../../models/Login";
import {Router} from "@angular/router";
import {IntegracionService} from "../../services/integracion.service";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-login-tiendas',
  templateUrl: './login-tiendas.component.html',
  styleUrls: ['./login-tiendas.component.css']
})
export class LoginTiendasComponent implements OnInit {

  @ViewChild('secondDialog') secondDialog: TemplateRef<any> | any;
  user = '';
  password = '';
  request: Login | undefined;


  openOtherDialog(): void {
    this.dialog.open(this.secondDialog);
  }

  newlogin(correo1: string, contra1: string): Login {
    return {
      email: correo1,
      contrasena: contra1
    };
  }

  constructor(private router: Router, private Integracion: IntegracionService, public Global: GlobalService,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  login_cliente(): void {
    this.request = this.newlogin(this.user, this.password);

    this.Integracion.login_cliente(this.request).subscribe(
      (res: any) => {
        if (res.status === "success") {
          this.Global.tipo = res.tipo;
          this.Global.nombre = res.data.nombre;
          this.Global.id = res.data.id;
          this.Global.correo = res.data.email;
          this.Global.universal = true;
          this.router.navigate(['/cliente-universal']);
        }
      },
      (error: any) => {
        console.error(error)
        alert("Error al ingresar a la tienda")
        this.user = '';
        this.password = '';
      }
    );
  }

  login_proveedor(): void {
    this.request = this.newlogin(this.user, this.password);

    this.Integracion.login_proveedor(this.request).subscribe(
      (res: any) => {
        if (res.status === "success") {
          this.Global.tipo = res.tipo;
          this.Global.nombre = res.data.nombre;
          this.Global.id = res.data.id;
          this.Global.correo = res.data.email;
          this.router.navigate(['/proveedor-universal']);
        }
      },
      (error: any) => {
        console.error(error)
        this.openOtherDialog();
        this.user = '';
        this.password = '';
      }
    );
  }

  onGroupChange(event: any, item: any): void{
    console.log(item)
    if(event.isUserInput){
      this.Global.API_BUS = item.API
    }
    console.log("BUS GLOBAL")
    console.log(this.Global.API_BUS)
  }

}
