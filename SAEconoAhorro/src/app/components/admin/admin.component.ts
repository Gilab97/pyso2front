import { Component, OnInit } from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import {formatNumberWithCommas} from '../../utilities/utilities';
import {Product} from '../../models/Product';
import {AdminService} from '../../services/admin.service';
import {GlobalService} from '../../services/global.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  formatNumber = formatNumberWithCommas
  loading = true
  cart = []
  idUser: any = 0
  totalOrders: any[] = []
  orders: any[] = []
  count = 0
  initial = 0

  constructor(private adminService: AdminService, public globalService: GlobalService, private router: Router) { }

  ngOnInit(): void {
    this.loading = true
    const request = this.globalService.id ? {id: `${this.globalService.id}`} : null
    // @ts-ignore
    this.adminService.getSales(request).subscribe(
      (res: {data: any[], status: number} | any) => {
        this.totalOrders = res.data
        this.orders = res.data.slice(0,20)
        this.count = res.data.length
        setTimeout(() => {this.loading = false},500)
    }, error => console.error(error))
  }

  show(id: number): void{
    this.router.navigate([`/dashboard/admin/${id}`])
  }

  pageEvent(event: PageEvent): void{
    const pos = (event.pageIndex+1) * event.pageSize
    this.orders = this.totalOrders.slice(pos - event.pageSize, (event.pageIndex+1) * event.pageSize)
  }

}
