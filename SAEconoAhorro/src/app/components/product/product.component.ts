import { Component, OnInit } from '@angular/core';
import {Product} from '../../models/Product';
import {ProductsService} from '../../services/products.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  product: Product = {}

  categories: {value: number, label: string}[] = [{value: 1, label: 'asdf'}, {value: 2, label: 'asdf'}, {value: 3, label: 'asdf'}]
  category = {}

  constructor(private productsService: ProductsService, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onCategoryChange(event: any, item: any): void{
    if(event.isUserInput){
      this.category = item
    }
  }

  create(): void{
    this.productsService.create(this.product).subscribe(
      res => {
        this.toastr.success("El producto ha sido creado exitosamente" ,this.product.nombre);
      },
      error => this.toastr.error("No se ha podido crear el producto" ,"Error"))
  }
}
