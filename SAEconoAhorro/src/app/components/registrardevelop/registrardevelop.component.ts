import { Component, OnInit } from '@angular/core';
import { prov } from '../../models/regproveedor';
import { RegistrarproveedorService } from '../../services/registrarproveedor.service';
import { MatDialog } from '@angular/material/dialog';
import { ViewChild, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-registrardevelop',
  templateUrl: './registrardevelop.component.html',
  styleUrls: ['./registrardevelop.component.css']
})
export class RegistrardevelopComponent implements OnInit {
  @ViewChild('secondDialog') secondDialog: TemplateRef<any> | any;
  hide = true;

  nombre = '';
  numero = '';
  correo = '';
  contra = '';
  request: prov | undefined;

  newprov(nombre1: string, apellido1: string, empresa1: string, direccion1: string, correo1: string, contra1: string): prov{
    return{
      nombre: nombre1,
      apellido: apellido1,
      empresa: empresa1,
      email: correo1,
      contrasena: contra1,
      direccion: direccion1
    };
  }

  constructor(private Registrarproveedor: RegistrarproveedorService, public dialog: MatDialog) { }


  ngOnInit(): void {
  }

  openOtherDialog(): void {
    this.dialog.open(this.secondDialog);
  }

  registro(){
    this.request = this.newprov('', '', this.nombre, this.numero, this.correo, this.contra);

    this.Registrarproveedor.registrar(this.request).subscribe(
      (res:any) => {
        if (res.status === "success"){
          this.openOtherDialog();
        }
      },
      error => console.error(error)
    );
  }

}
