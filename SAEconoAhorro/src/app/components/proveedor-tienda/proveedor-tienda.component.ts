import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../services/global.service";
import {IntegracionService} from "../../services/integracion.service";

@Component({
  selector: 'app-proveedor-tienda',
  templateUrl: './proveedor-tienda.component.html',
  styleUrls: ['./proveedor-tienda.component.css']
})
export class ProveedorTiendaComponent implements OnInit {

  nombre = '';
  descripcion = '';
  stock = 0;
  precio_venta = 0;
  direccionURL = '';
  user = {id_user: 0, base64: '', extension: '', correo: ''};
  showSelector = true;
  file: any;


  constructor(public integracion: IntegracionService, public globalService: GlobalService) { }

  ngOnInit(): void {
    this.showSelector=true
  }

  async CrearProducto() {
    // @ts-ignore
    this.user.id_user = this.globalService.id
    // @ts-ignore
    this.user.correo = this.globalService.correo
    let objeto: any = {}
    await this.integracion.cargar_imagen(this.user).subscribe(
      (res: any) => {
        this.direccionURL = res.direccionURL;
        objeto = {
          id_proveedor: this.globalService.id,
          nombre: this.nombre,
          descripcion: this.descripcion,
          stock: this.stock,
          precio_venta: this.precio_venta,
          foto: this.direccionURL
        }
        console.log(objeto)
        this.integracion.crear_producto(objeto).subscribe(
          (res: any) => {
            alert("El producto se creo satisfactoriamente")
          }, error => {
            console.log(error)
          })
      }, error => {
        console.log(error)
      }
      );
  }




  changeListener($event: any): void {
    this.showSelector = false
    this.file = $event.target.files[0];
    this.getFile(this.file);
  }

  getFile(newFile: any): void{
    const reader = new FileReader();
    reader.readAsDataURL(newFile);
    reader.onload = (e) => {
      this.user.base64 = reader.result as string;
      this.user.base64 = this.user.base64.split(',')[1];
      this.user.extension = newFile.name.split('.')[1];
      this.chargeImage(this.user.base64, this.user.extension)
    }
  }

  chargeImage(base64: any, extension: any): void{
    const image = document.getElementById('image') as HTMLImageElement;
    image.src = `data:image/${extension};base64, ${base64}`
  }

}
