import { Component, OnInit } from '@angular/core';
import { cliente } from '../../models/regcliente';
import { IntegracionService } from '../../services/integracion.service';
import { MatDialog } from '@angular/material/dialog';
import { ViewChild, TemplateRef } from '@angular/core';
import {GlobalService} from "../../services/global.service";

@Component({
  selector: 'app-registrar-cliente-tienda',
  templateUrl: './registrar-cliente-tienda.component.html',
  styleUrls: ['./registrar-cliente-tienda.component.css']
})
export class RegistrarClienteTiendaComponent implements OnInit {
  @ViewChild('secondDialog') secondDialog: TemplateRef<any> | any;
  hide = true;

  nombre = '';
  apellido = '';
  correo = '';
  contra = '';
  celular = 0;
  request: cliente | undefined;

  newprov(nombre1: string, apellido1: string, correo1: string, contra1: string, celular1: number): cliente{
    return{
      nombre: nombre1,
      apellido: apellido1,
      email: correo1,
      contrasena: contra1,
      celular: celular1
    };
  }

  constructor(private integracino: IntegracionService, public dialog: MatDialog, public Global: GlobalService) { }

  ngOnInit(): void {
  }

  openOtherDialog(): void {
    this.dialog.open(this.secondDialog);
  }

  registro(): void{
    this.request = this.newprov(this.nombre, this.apellido, this.correo, this.contra, this.celular);

    this.integracino.registrar_cliente(this.request).subscribe(
      (res: any) => {
        if (res.status === "success"){
          this.openOtherDialog();
        }
      },
      error => console.error(error)
    );
  }

  onGroupChange(event: any, item: any): void{
    if(event.isUserInput){
      this.Global.API_BUS = item.API
    }
    console.log(this.Global.API_BUS)
  }

}
