import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../services/admin.service';
import {ActivatedRoute} from '@angular/router';
import {formatNumberWithCommas} from '../../utilities/utilities';
import {ToastrService} from 'ngx-toastr';
import {GlobalService} from '../../services/global.service';
import {CartService} from '../../services/cart.service';
import { email } from '../../models/Email';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  order: any = {}
  detail: any[] = []
  displayedColumns = ['nombre', 'Cantidad', 'SubTotal'];
  formatNumber = formatNumberWithCommas
  states: {id: number, nombre: string}[] = [{id: 1, nombre: 'Ingresada'}, {id: 2, nombre: 'Procesada'}, {id: 3, nombre: 'Empaquetada'},
    {id: 4, nombre: 'En tienda'}, {id: 5, nombre: 'En camino'}, {id: 6, nombre: 'Recibida'}]
  id = 0

  constructor(private adminService: AdminService, private activatedRoute: ActivatedRoute, private toastr: ToastrService,
              public globalService: GlobalService, private cartService: CartService) { }

  ngOnInit(): void {
    this.id = this.globalService.id || 0
    const id = this.activatedRoute.snapshot.paramMap.get('id') || 0;
    this.adminService.getSalesById(Number(id)).subscribe((res: any) => {
      this.order = res.data.order;
      this.detail = res.data.detail
      if(!this.order.entrega)
        this.states = this.states.filter((f, index) => this.order.estado <= (index+1) && index < 4)
      else
        this.states = this.states.filter((f, index) => this.order.estado <= (index+1))
    }, error => console.error(error))
  }

  onStateChange(event: any, item: any): void{
    if(event.isUserInput){
      this.order.estado = item.id
    }
  }

  update(): void{
    const request = {estado: this.order.estado}
    this.adminService.updateStatus(this.order.idVenta, request).subscribe(
      (res) => {
        if(this.order.estado === 4 && !this.order.entrega){
          const e: email = {email: this.order.Correo, body: `La compra ${this.order.idVenta} está en tienda`, header: 'Compra lista',
            subject: 'Compra lista'}
          this.cartService.sendEmailConfirmation(e).subscribe((res: any) => {
            console.log(res)
          }, error => this.toastr.error('Error al enviar la notificación al cliente'))
        }
        if(this.order.estado === 6 && this.order.entrega){
          const e: email = {email: this.order.Correo, body: `La compra ${this.order.idVenta} fue entregada`, header: 'Compra entregada',
            subject: 'Compra entregada'}
          this.cartService.sendEmailConfirmation(e).subscribe((res: any) => {
            console.log(res)
          }, error => this.toastr.error('Error al enviar la notificación al cliente'))
        }
        this.toastr.success('Estado actualizado', 'Venta')
    }, error => this.toastr.error('Error al actualizar el estado', 'Venta'))
  }

}
