import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {PerfilService} from "../../services/perfil.service";
import {GlobalService} from "../../services/global.service";
import {MatDialog} from "@angular/material/dialog";
import {TarjetasService} from "../../services/tarjetas.service";

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styleUrls: ['./tarjetas.component.css']
})

export class TarjetasComponent implements OnInit {

  CARDS: any;
  tarjeta = '';
  nombre = '';

  constructor(private router: Router, private tarjetasServices: TarjetasService, public Global: GlobalService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.tarjetasServices.getCards(this.Global.id).subscribe(
      res=>{
        this.CARDS = res
        console.log(this.CARDS)
      }, err=>{
        console.log(err)
      }
    )
  }

  newCard(): void{
    const objeto = {NoTarjeta: this.tarjeta, Cliente: this.Global.id, Nombre: this.nombre}
    this.tarjetasServices.newCard(objeto).subscribe(
      res=>{
        alert("Se agrego la tarjeta correctamente")
        this.ngOnInit()
      },err=>{
        alert("Ocurrio un error al agregar la tarjeta")
        console.log(err)
      }
    )
  }
}
