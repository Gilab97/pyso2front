import { Component, OnInit } from '@angular/core';
import {formatNumberWithCommas, getCart, addCart} from '../../utilities/utilities';
import {SubastaService} from '../../services/subasta.service';
import {Subasta} from '../../models/Subasta';
import {Router} from '@angular/router';
import {GlobalService} from '../../services/global.service';
import {Product} from '../../models/Product';
import {ProductsService} from '../../services/products.service';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-dashboard-subasta',
  templateUrl: './dashboard-subasta.component.html',
  styleUrls: ['./dashboard-subasta.component.css']
})
export class DashboardSubastaComponent implements OnInit {

  formatNumber = formatNumberWithCommas
  loading = true
  subastas: Subasta[] = []

  constructor(private subastaService: SubastaService, private router: Router, public globalService: GlobalService,
              private productService: ProductsService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.subastaService.getSubastas().subscribe((res: Subasta[] | any) => {
      this.subastas = res
    })
  }

  verSubasta(item: Subasta): void{
    this.productService.getSingle(item.producto_id || 0).subscribe((res: Product[] | any) => {
      let product = res[0]
      product = {...product, precio: item.precio_inicial, quantity: 1, subasta: item.id, select: true}
      addCart(product, this.globalService.id || 0)
      this.toastr.success("Producto agregado al carrito","Agregado al Carrito")
    })
    // this.router.navigate([`/subasta/${item.id}`])
  }
}
