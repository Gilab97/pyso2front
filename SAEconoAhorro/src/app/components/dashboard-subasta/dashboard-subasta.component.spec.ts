import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSubastaComponent } from './dashboard-subasta.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Routes} from '@angular/router';
import {OrderComponent} from '../order/order.component';
import {SubastaComponent} from '../subasta/subasta.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ToastrModule, ToastrService} from 'ngx-toastr';

describe('DashboardSubastaComponent', () => {
  let component: DashboardSubastaComponent;
  let fixture: ComponentFixture<DashboardSubastaComponent>;

  const routes: Routes = [
    {
      path: 'subasta/:id',
      component: SubastaComponent
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardSubastaComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(routes), ToastrModule.forRoot()],
      providers: [ToastrService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSubastaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
