import { Component, OnInit } from '@angular/core';
import { cliente } from '../../models/regcliente';
import { RegistrarclienteService } from '../../services/registrarcliente.service';
import { MatDialog } from '@angular/material/dialog';
import { ViewChild, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {
  @ViewChild('secondDialog') secondDialog: TemplateRef<any> | any;
  hide = true;

  nombre = '';
  apellido = '';
  correo = '';
  contra = '';
  celular = 0;
  request: cliente | undefined;

  newprov(nombre1: string, apellido1: string, correo1: string, contra1: string, celular1: number): cliente{
    return{
      nombre: nombre1,
      apellido: apellido1,
      email: correo1,
      contrasena: contra1,
      celular: celular1
    };
  }

  constructor(private Registrarcliente: RegistrarclienteService, public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openOtherDialog(): void {
    this.dialog.open(this.secondDialog);
  }

  registro(): void{
    this.request = this.newprov(this.nombre, this.apellido, this.correo, this.contra, this.celular);

    this.Registrarcliente.registrar(this.request).subscribe(
      (res: any) => {
        if (res.status === "success"){
          this.openOtherDialog();
        }
      },
      error => console.error(error)
    );
  }

}
