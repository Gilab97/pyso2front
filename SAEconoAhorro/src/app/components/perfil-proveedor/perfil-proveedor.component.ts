import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {PerfilService} from "../../services/perfil.service";
import {GlobalService} from "../../services/global.service";
import {MatDialog} from "@angular/material/dialog";
import {Perfil} from "../../models/Perfil";

@Component({
  selector: 'app-perfil-proveedor',
  templateUrl: './perfil-proveedor.component.html',
  styleUrls: ['./perfil-proveedor.component.css']
})
export class PerfilProveedorComponent implements OnInit {
  nombre = '';
  correo = '';
  pass = '';
  direccion = '';
  datos = {nombre: '', correo: '', pass: '', direccion: ''};
  cantidad = 0;
  ventas = 0;

  constructor(private router: Router, private perfilService: PerfilService, public Global: GlobalService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.perfilService.getProviderProfile(this.Global.id).subscribe(
      res => {
        // @ts-ignore
        this.datos = res[0];
        console.log(this.datos)
        this.setFields();
      },
      err => { console.log(err); }
    );

    this.perfilService.getReport(this.Global.id).subscribe(
      (res: {cantidad: any, valor: any} | any) => {
        this.cantidad = res.cantidad;
        this.ventas = res.valor;
      },
      err => {
        console.log(err);
      }
    );
  }

  updateProfile() {
    const objeto = {nombre: this.nombre, correo: this.correo, pass: this.pass, direccion: this.direccion};
    this.perfilService.updateProviderProfile(objeto).subscribe(
      res => {
        alert("Se actualizo la informacion");
      }, err => {
        alert("Ocurrio un error al actualizar los datos");
        console.log(err)
      }
    )
  }

  setFields(){
    this.nombre = this.datos.nombre;
    this.correo = this.datos.correo;
    this.pass = this.datos.pass;
    this.direccion = this.datos.direccion;
  }

}
