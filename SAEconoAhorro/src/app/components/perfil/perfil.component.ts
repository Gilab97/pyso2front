import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {PerfilService} from "../../services/perfil.service";
import {GlobalService} from "../../services/global.service";
import {MatDialog} from "@angular/material/dialog";
import {Perfil} from "../../models/Perfil";

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  profilePic = '';
  nombres = '';
  apellidos = '';
  correo = '';
  pass = '';
  celular = '';
  fotografia = '';
  datos = {Fotografia: '', Nombres: '', Apellidos: '', Correo: '', Pass: '', Celular: ''};

  user = {id_user: 0, base64: '', extension: '', correo: ''};
  showSelector = true;
  file: any;

  constructor(private router: Router, private perfilService: PerfilService, public Global: GlobalService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.showSelector=true
    this.perfilService.getProfile(this.Global.id).subscribe(
      res => {
        // @ts-ignore
        this.datos = res[0];
        this.setFields();
      },
        err => { console.log(err); }
    );
  }

  updateProfile(): void {
    const objeto: Perfil = {Nombres: this.nombres, Apellidos: this.apellidos, Correo: this.correo, Pass: this.pass, Celular: this.celular};
    this.perfilService.updateProfile(objeto).subscribe(
      res => {
        alert("Se actualizo la informacion");
      }, err => {
      alert("Ocurrio un error al actualizar los datos");
      console.log(err)
    }
    )
  }

  updatePicture(): void {
    this.user.id_user = this.Global.id || 0;
    this.user.correo = this.correo;
    this.perfilService.updatePictura(this.user).subscribe(
      res=>{
        alert("Se actualizo la foto exitosamente");
        this.ngOnInit()
      },err=>{
        alert("Ocurrio un error al actualizar la foto");
        console.log(err);
      }
    )
  }

  setFields(): void{
    this.profilePic = this.datos.Fotografia;
    this.nombres = this.datos.Nombres;
    this.apellidos = this.datos.Apellidos;
    this.correo = this.datos.Correo;
    this.pass = this.datos.Pass;
    this.celular = this.datos.Celular;
  }

  changeListener($event: any): void {
    this.showSelector = false
    this.file = $event.target.files[0];
    this.getFile(this.file);
  }

  getFile(newFile: any): void{
    const reader = new FileReader();
    reader.readAsDataURL(newFile);
    reader.onload = (e) => {
      this.user.base64 = reader.result as string;
      this.user.base64 = this.user.base64.split(',')[1];
      this.user.extension = newFile.name.split('.')[1];
      this.chargeImage(this.user.base64, this.user.extension)
    }
  }

  chargeImage(base64: any, extension: any): void{
    const image = document.getElementById('image') as HTMLImageElement;
    image.src = `data:image/${extension};base64, ${base64}`
  }

}
