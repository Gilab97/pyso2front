import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Login } from '../../models/Login';
import { LoginService } from '../../services/login.service';
import { GlobalService } from '../../services/global.service';
import {Router} from '@angular/router';
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('secondDialog') secondDialog: TemplateRef<any> | any;
  user = '';
  password = '';
  request: Login | undefined;

  newlogin(correo1: string, contra1: string): Login{
    return{
      email: correo1,
      contrasena: contra1
    };
  }

  constructor(private router: Router, private Login: LoginService, private Global: GlobalService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openOtherDialog(): void {
    this.dialog.open(this.secondDialog);
  }

  login(): void{
    if(this.user === 'admin@econoahorro.com' && this.password === 'admin'){
      this.Global.tipo = 0;
      this.Global.nombre = 'admin';
      this.Global.id = 0;
      this.Global.correo = 'admin@econoahorro.com';
      this.Global.universal = false;
      this.router.navigate(['/dashboard/admin'])
      return
    }
    this.request = this.newlogin(this.user, this.password);

    this.Login.registrar(this.request).subscribe(
      (res: any) => {
        console.log(res)
        if(res.status ===  "success"){
          this.Global.tipo = res.tipo;
          this.Global.nombre = res.data.nombre;
          this.Global.id = res.data.id;
          this.Global.correo = res.data.email;
          this.Global.universal = false;
          if (res.tipo == 1){
          /*console.log(this.Global.tipo);
            console.log(this.Global.nombre);
            console.log(this.Global.id);
            console.log(this.Global.correo);*/
            this.router.navigate(['/dashboard']);
          }else {
            this.router.navigate(['/dashboard/productos']);
          }
        }
      },
      error =>{
        console.error(error)
        this.openOtherDialog();
        this.user = '';
        this.password = '';
      }
    );
  }


}
