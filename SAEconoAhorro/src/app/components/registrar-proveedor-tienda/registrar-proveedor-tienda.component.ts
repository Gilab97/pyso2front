import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {prov} from "../../models/regproveedor";
import {IntegracionService} from "../../services/integracion.service";
import {MatDialog} from "@angular/material/dialog";
import {GlobalService} from "../../services/global.service";

@Component({
  selector: 'app-registrar-proveedor-tienda',
  templateUrl: './registrar-proveedor-tienda.component.html',
  styleUrls: ['./registrar-proveedor-tienda.component.css']
})
export class RegistrarProveedorTiendaComponent implements OnInit {
  @ViewChild('secondDialog') secondDialog: TemplateRef<any> | any;
  hide = true;

  nombre = '';
  numero = '';
  correo = '';
  contra = '';
  request: prov | undefined;

  newprov(nombre1: string, apellido1: string, empresa1: string, direccion1: string, correo1: string, contra1: string): prov{
    return{
      nombre: nombre1,
      apellido: apellido1,
      empresa: empresa1,
      email: correo1,
      contrasena: contra1,
      direccion: direccion1
    };
  }

  constructor(private integracion: IntegracionService, public dialog: MatDialog,  public Global: GlobalService) { }


  ngOnInit(): void {
  }

  openOtherDialog(): void {
    this.dialog.open(this.secondDialog);
  }

  registro(){
    this.request = this.newprov(this.nombre, '', this.nombre, this.numero, this.correo, this.contra);

    this.integracion.registrar_proveedor(this.request).subscribe(
      (res: any) => {
        if (res.status === "success"){
          this.openOtherDialog();
        }
      },
      error => console.error(error)
    );
  }

  onGroupChange(event: any, item: any): void{
    if(event.isUserInput){
      this.Global.API_BUS = item.API
    }
    console.log(this.Global.API_BUS)
  }

}
