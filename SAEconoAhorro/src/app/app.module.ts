import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from "@angular/material/input";
import { MatCardModule } from "@angular/material/card";
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import { ProductComponent } from './components/product/product.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ErrorComponent } from './components/error/error.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import { RegistrardevelopComponent } from './components/registrardevelop/registrardevelop.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from "@angular/material/dialog";
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatChipsModule} from '@angular/material/chips';
import {MatOptionModule} from '@angular/material/core';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatSelectModule} from '@angular/material/select';
import { ToastrModule } from 'ngx-toastr';
import { ProductTableComponent } from './components/product-table/product-table.component';
import {MatTableModule} from '@angular/material/table';
import { DatePipe } from '@angular/common'

import { PerfilComponent } from './components/perfil/perfil.component';
import { PerfilProveedorComponent } from './components/perfil-proveedor/perfil-proveedor.component';
import { TarjetasComponent } from './components/tarjetas/tarjetas.component';
import { ComprasClienteComponent } from './components/compras-cliente/compras-cliente.component';
import { VentasProveedorComponent } from './components/ventas-proveedor/ventas-proveedor.component';
import { SubastaComponent } from './components/subasta/subasta.component';
import { FavoriteComponent } from './components/favorite/favorite.component';
import { DashboardSubastaComponent } from './components/dashboard-subasta/dashboard-subasta.component';
import { PerfilSubastaComponent } from './components/perfil-subasta/perfil-subasta.component';
import { LoginTiendasComponent } from './components/login-tiendas/login-tiendas.component';
import { RegistrarClienteTiendaComponent } from './components/registrar-cliente-tienda/registrar-cliente-tienda.component';
import { RegistrarProveedorTiendaComponent } from './components/registrar-proveedor-tienda/registrar-proveedor-tienda.component';
import { ProveedorTiendaComponent } from './components/proveedor-tienda/proveedor-tienda.component';
import { ClienteTiendaComponent } from './components/cliente-tienda/cliente-tienda.component';
import { AdminComponent } from './components/admin/admin.component';
import { OrderComponent } from './components/order/order.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginComponent,
    RegistrarComponent,
    ProductComponent,
    DashboardComponent,
    ErrorComponent,
    RegistrardevelopComponent,
    ShoppingCartComponent,
    ProductTableComponent,
    RegistrardevelopComponent,
    PerfilComponent,
    PerfilProveedorComponent,
    TarjetasComponent,
    ComprasClienteComponent,
    VentasProveedorComponent,
    TarjetasComponent,
    SubastaComponent,
    FavoriteComponent,
    DashboardSubastaComponent,
    PerfilSubastaComponent,
    LoginTiendasComponent,
    RegistrarClienteTiendaComponent,
    RegistrarProveedorTiendaComponent,
    ProveedorTiendaComponent,
    ClienteTiendaComponent,
    PerfilSubastaComponent,
    AdminComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatChipsModule,
    ReactiveFormsModule,
    MatOptionModule,
    MatAutocompleteModule,
    DragDropModule,
    MatSelectModule,
    ToastrModule.forRoot(),
    MatTableModule,
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
