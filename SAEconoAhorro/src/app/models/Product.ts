export interface Product {
  id?: number;
  nombre?: string;
  proveedor?: string;
  categoria?: string;
  proveedor_id?: number;
  categoria_id?: number;
  imagen?: string;
  quantity?: number;
  stock?: number;
  precio?: number;
  select?: boolean;
  subasta?: number;
}
