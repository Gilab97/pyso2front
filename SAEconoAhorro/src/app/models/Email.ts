export interface email{
    email: String;
    header: String;
    body: String;
    subject: String;
}