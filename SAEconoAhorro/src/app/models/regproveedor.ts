export interface prov{
  nombre: string;
  apellido: string;
  empresa: string;
  email: string;
  contrasena: string;
  direccion: string;
}
