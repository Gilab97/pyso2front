export interface cliente{
  nombre: string;
  apellido: string;
  email: string;
  contrasena: string;
  celular: number;
}
