export interface detalleVenta{
    venta: number;
    producto: number;
    cantidad: number;
    subTotal: number;
    subasta?: number;
}
