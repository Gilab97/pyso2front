export interface Subasta{
  precio_inicial?: number;
  tiempo?: number;
  titulo?: string;
  descripcion?: string;
  hora?: number;
  minuto?: number;
  proveedor_id?: number,
  producto_id?: number
  id?: number
}
