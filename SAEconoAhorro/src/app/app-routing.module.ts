import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import {ProductComponent} from './components/product/product.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {ErrorComponent} from './components/error/error.component';
import { RegistrardevelopComponent } from './components/registrardevelop/registrardevelop.component';
import { ShoppingCartComponent } from './components/shopping-cart/shopping-cart.component';
import {ProductTableComponent} from './components/product-table/product-table.component';
import {PerfilComponent} from "./components/perfil/perfil.component";
import {PerfilProveedorComponent} from "./components/perfil-proveedor/perfil-proveedor.component";
import {TarjetasComponent} from "./components/tarjetas/tarjetas.component";
import {ComprasClienteComponent} from "./components/compras-cliente/compras-cliente.component";
import {VentasProveedorComponent} from "./components/ventas-proveedor/ventas-proveedor.component";
import {SubastaComponent} from './components/subasta/subasta.component';
import {DashboardSubastaComponent} from './components/dashboard-subasta/dashboard-subasta.component';
import {PerfilSubastaComponent} from './components/perfil-subasta/perfil-subasta.component';
import {FavoriteComponent} from './components/favorite/favorite.component';
import {LoginTiendasComponent} from "./components/login-tiendas/login-tiendas.component";
import {RegistrarClienteTiendaComponent} from "./components/registrar-cliente-tienda/registrar-cliente-tienda.component";
import {RegistrarProveedorTiendaComponent} from "./components/registrar-proveedor-tienda/registrar-proveedor-tienda.component";
import {ClienteTiendaComponent} from "./components/cliente-tienda/cliente-tienda.component";
import {ProveedorTiendaComponent} from "./components/proveedor-tienda/proveedor-tienda.component";
import {AdminComponent} from './components/admin/admin.component';
import {OrderComponent} from './components/order/order.component';

const routes: Routes = [
  {
    path: '', component: WelcomeComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'login-tiendas', component: LoginTiendasComponent
  },
  {
    path: 'cliente-universal', component: ClienteTiendaComponent
  },
  {
    path: 'proveedor-universal', component: ProveedorTiendaComponent
  },
  {
    path: 'login-tiendas', component: LoginTiendasComponent
  },
  {
    path: 'registrar', component: RegistrarComponent
  },
  {
    path: 'cliente-tienda', component: RegistrarClienteTiendaComponent
  },
  {
    path: 'proveedor-tienda', component: RegistrarProveedorTiendaComponent
  },
  {
    path: 'dashboard', component: DashboardComponent
  },
  {
    path: 'dashboard/productos', component: ProductTableComponent
  },
  {
    path: 'dashboard/productos/:id', component: ProductComponent
  },
  {
    path: 'proveedor', component: RegistrardevelopComponent
  },
  {
    path: 'shoppingcart', component: ShoppingCartComponent
  },
  {
    path: 'product-table', component: ProductTableComponent
  },
  {
    path: 'perfil/:id', component: PerfilComponent
  },
  {
    path: 'perfil_proveedor/:id', component: PerfilProveedorComponent
  },
  {
    path: 'tarjetas/:id', component: TarjetasComponent
  },
  {
    path: 'reporte_cliente/:id', component: ComprasClienteComponent
  },
  {
    path: 'reporte_proveedor/:id', component: VentasProveedorComponent
  },
  {
    path: 'producto/subasta/:id', component: SubastaComponent
  },
  {
    path: 'dashboard/subastas', component: DashboardSubastaComponent
  },
  {
    path: 'subastas/:id', component: PerfilSubastaComponent
  },
  {
    path: 'favoritos/:id', component: FavoriteComponent
  },
  {
    path: 'dashboard/admin', component: AdminComponent
  },
  {
    path: 'dashboard/admin/:id', component: OrderComponent
  },
  {
    path: '**', component: ErrorComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
